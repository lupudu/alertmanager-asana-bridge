package config

import (
	"io/ioutil"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

type AsanaConfig struct {
	AccessToken string `yaml:"access_token"`
	Workspace   string `yaml:"workspace"`
	Project     string `yaml:"project"`

	Assignee string `yaml:"assignee"`
}

type WebHookConfig struct {
	Listen string `yaml:"listen"`
}

type Config struct {
	Asana   AsanaConfig   `yaml:"asana"`
	WebHook WebHookConfig `yaml:"webhook"`
}

func Parse(filePath string) *Config {
	// Check if the config file exists and is not empty
	info, err := os.Stat(filePath)
	if os.IsNotExist(err) {
		log.Fatalf("Config file %v does not exist.", filePath)
	}
	if info.Size() == 0 {
		log.Fatalf("Config file %v is empty.", filePath)
	}

	log.Printf("Load config file %s", filePath)

	yamlFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		log.Fatalf("Can not read config file: %v", err)
	}

	config := &Config{}

	err = yaml.Unmarshal(yamlFile, config)
	if err != nil {
		log.Fatalf("Can not parse config file: %v", err)
	}

	return config
}
