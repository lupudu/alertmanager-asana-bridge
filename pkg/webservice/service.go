package webservice

import (
	"log"
	"net/http"

	"lupudu.de/alertmanager-asana-bridge/pkg/config"
)

func Start(cfg *config.Config) {
	asanaHandler := AsanaHandler{
		Config: cfg,
	}
	http.Handle("/", asanaHandler)

	log.Printf("Listen on %v, waiting for webhooks ... ", cfg.WebHook.Listen)
	err := http.ListenAndServe(cfg.WebHook.Listen, nil)
	if err != nil {
		log.Fatalf("Can not start webserver: %v", err)
	}
}
