package webservice

import (
	"bitbucket.org/mikehouston/asana-go"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"sync"

	"github.com/deckarep/golang-set"
	"github.com/prometheus/alertmanager/template"
	"lupudu.de/alertmanager-asana-bridge/pkg/config"
)

var asanaClient *asana.Client
var asanaClientOnce sync.Once

func createAsanaClientOnce(accessToken string) {
	asanaClientOnce.Do(func() {
		asanaClient = asana.NewClientWithAccessToken(accessToken)
	})
}

type AsanaHandler struct {
	Config *config.Config
}

// activeAlerts keeps track of all the active alerts, so we can decide
// which alert we actually use to create the Asana task and which one we can
// drop right away.
var activeAlerts = mapset.NewSet()

// Some references
// https://github.com/tomtom-international/alertmanager-webhook-logger/blob/master/main.go
// https://github.com/inCaller/prometheus_bot/blob/master/main.go
// https://prometheus.io/docs/operating/integrations/#alertmanager-webhook-receiver

func (h AsanaHandler) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	var alerts template.Data
	err := json.NewDecoder(req.Body).Decode(&alerts)
	if err != nil {
		log.Println("Can't understand request.")
		http.Error(res, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("New Alert comming in: %v", alerts.CommonAnnotations["summary"])

	// TODO: Debugging
	s, _ := json.MarshalIndent(alerts, "", "\t")
	fmt.Print(string(s))

	key := alerts.GroupLabels["alertname"] + alerts.GroupLabels["instance"]

	// We drop resolved alerts for now because we cannot close Asana tasks
	// automatically. But we can use it to keep our list of "hot" alerts clean.
	if len(alerts.Alerts.Firing()) == 0 {
		log.Println("No firing alert, ignore this webhook.")
		if activeAlerts.Contains(key) {
			activeAlerts.Remove(key)
		}
	}

	title := "[" + alerts.CommonLabels["severity"] + "] " + alerts.CommonAnnotations["summary"]

	description := ""
	for _, alert := range alerts.Alerts.Firing() {
		description = description + alert.Annotations["summary"] + "\n"
	}

	// Deduplicate the alert. We need to add a similar alert only once, because
	// Asana is a ticket system, not an alerting tool. For that, we need to detect
	// if an alert is already made. And we don't like to make any persistent
	// "state".
	if activeAlerts.Contains(key) {
		log.Println("We already alerted this alert, so we will drop it.")
		res.WriteHeader(http.StatusNoContent)
		return
	}

	activeAlerts.Add(key)
	go createAsanaTask(
		h.Config,
		title,
		description,
	)

	res.WriteHeader(http.StatusNoContent)
}

func createAsanaTask(cfg *config.Config, title string, description string) {
	log.Println("Create new Asana Alert ...")

	createAsanaClientOnce(cfg.Asana.AccessToken)
	_, err := asanaClient.CreateTask(&asana.CreateTaskRequest{
		Assignee:  cfg.Asana.Assignee,
		Workspace: cfg.Asana.Workspace,
		Projects: []string{
			cfg.Asana.Project,
		},
		TaskBase: asana.TaskBase{
			Name:  title,
			Notes: description,
		},
	})

	if err != nil {
		log.Fatalf("Can not create task: %v", err.Error())
	}
}
