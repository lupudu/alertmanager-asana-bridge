# Alertmanager Asana Bridge

Provide a webhook to create Asana tasks from Prometheus Alertmanager. This is
useful if you want to create Tasks from Alertmanager Alerts.

## Usage

1. Edit the config file
2. Start the alertmanager-asana-bridge with the -config flag
3. Configure alertmanager to send alerts to this webhook
4. Check Asana for new alerts
