package main

// Simple bridge between the Prometheus Alert Manager (webhooks) and Asana
// (tasks). Use this to provide a webhook to send ne alerts in (the Alert
// Manager provides a webhook alerter). If the json body matches an alert,
// a new Asana task will be created. Sadly, we cannot match resolved alerts,
// to automatically close an Asana task when the alert is resolved.

import (
	"flag"

	"lupudu.de/alertmanager-asana-bridge/pkg/config"
	"lupudu.de/alertmanager-asana-bridge/pkg/webservice"
)

var cfg *config.Config

func main() {
	webservice.Start(cfg)
}

func init() {
	var configFile = flag.String("config", "alertmanager-asana-bridge.config.yml", "The configuration file")
	flag.Parse()

	cfg = config.Parse(*configFile)
}
