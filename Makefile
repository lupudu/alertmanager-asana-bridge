.PHONY: build-arm build build-all package run clean demo-alert

binary = alertmanager-asana-bridge

build-arm:
	mkdir -p bin
	env GOOS=linux GOARCH=arm GOARM=5 CGO_ENABLED=0 go build -o bin/$(binary)-arm cmd/$(binary)/main.go

build:
	mkdir -p bin
	CGO_ENABLED=0 go build -o bin/$(binary) cmd/$(binary)/main.go

build-all: build build-arm

package: build build-arm
	mkdir -p lupudu-$(binary)
	cp bin/$(binary) lupudu-$(binary)/
	cp bin/$(binary)-arm lupudu-$(binary)/
	tar czf lupudu-$(binary).tar.gz lupudu-$(binary)
	rm -rf lupudu-$(binary)

run:
	go run cmd/$(binary)/main.go -config alertmanager-asana-bridge.config

demo-alert:
	curl -XPOST -d @examples/demo-alert.json http://localhost:9097/

clean:
	rm -rf bin lupudu-$(binary).tar.gz
